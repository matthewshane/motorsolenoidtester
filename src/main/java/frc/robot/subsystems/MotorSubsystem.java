// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;
import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

public class MotorSubsystem extends SubsystemBase {

  private WPI_TalonSRX m_motorTalon1;
  private WPI_TalonSRX m_motorTalon2;

  private CANSparkMax m_motorSpark1;
  private CANSparkMax m_motorSpark2;

  private int m_portMotorTalon1 = 0;
  private int m_portMotorTalon2 = 1;
  private int m_portMotorSpark1 = 2;
  private int m_portMotorSpark2 = 3;

  /** Creates a new MotorSubsystem. */
  public MotorSubsystem() {
    SmartDashboard.putBoolean("Motor Talon1 Enabled", false);
    SmartDashboard.putNumber("Motor Talon1 Port", m_portMotorTalon1);
    SmartDashboard.putNumber("Motor Talon1 Speed", 0);

    SmartDashboard.putBoolean("Motor Talon2 Enabled", false);
    SmartDashboard.putNumber("Motor Talon2 Port", m_portMotorTalon2);
    SmartDashboard.putNumber("Motor Talon2 Speed", 0);

    SmartDashboard.putBoolean("Motor Spark1 Enabled", false);
    SmartDashboard.putNumber("Motor Spark1 Port", m_portMotorSpark1);
    SmartDashboard.putNumber("Motor Spark1 Speed", 0);

    SmartDashboard.putBoolean("Motor Spark2 Enabled", false);
    SmartDashboard.putNumber("Motor Spark2 Port", m_portMotorSpark2);
    SmartDashboard.putNumber("Motor Spark2 Speed", 0);
  }

  @Override
  public void periodic() {
    boolean enabled = false;
    int desiredPort = 0;
    double desiredSpeed = 0;

    // Below is the code for controlling the motor Talon1.
    enabled = SmartDashboard.getBoolean("Motor Talon1 Enabled", false);
    if (enabled && m_motorTalon1 == null) {
      m_motorTalon1 = new WPI_TalonSRX((int) SmartDashboard.getNumber("Motor Talon1 Port", m_portMotorTalon1));
    } else if (!enabled && m_motorTalon1 != null) {
      m_motorTalon1.set(0.0);
      m_motorTalon1.close();
      m_motorTalon1 = null;
    }

    desiredPort = (int) SmartDashboard.getNumber("Motor Talon1 Port", m_portMotorTalon1);
    if (desiredPort != m_portMotorTalon1 && enabled) {
      m_motorTalon1 = new WPI_TalonSRX(desiredPort);
      m_portMotorTalon1 = desiredPort;
    }

    desiredSpeed = SmartDashboard.getNumber("Motor Talon1 Speed", 0);
    if ((desiredSpeed < -1.0 || desiredSpeed > 1.0) && enabled) {
      m_motorTalon1.set(0.0);
      SmartDashboard.putNumber("Motor Talon1 Speed", 0);
    } else if (enabled) {
      m_motorTalon1.set(desiredSpeed);
      SmartDashboard.putNumber("in else if", desiredSpeed);
    }

    // Below is the code for controlling the motor Talon2.
    enabled = SmartDashboard.getBoolean("Motor Talon2 Enabled", false);
    if (enabled && m_motorTalon2 == null) {
      m_motorTalon2 = new WPI_TalonSRX((int) SmartDashboard.getNumber("Motor Talon2 Port", m_portMotorTalon2));
    } else if (!enabled && m_motorTalon2 != null) {
      m_motorTalon2.set(0.0);
      m_motorTalon2.close();
      m_motorTalon2 = null;
    }

    desiredPort = (int) SmartDashboard.getNumber("Motor Talon2 Port", m_portMotorTalon2);
    if (desiredPort != m_portMotorTalon2 && enabled) {
      m_motorTalon2 = new WPI_TalonSRX(desiredPort);
      m_portMotorTalon2 = desiredPort;
    }

    desiredSpeed = SmartDashboard.getNumber("Motor Talon2 Speed", 0);
    if ((desiredSpeed < -1.0 || desiredSpeed > 1.0) && enabled) {
      m_motorTalon2.set(0.0);
      SmartDashboard.putNumber("Motor Talon2 Speed", 0);
    } else if (enabled) {
      m_motorTalon2.set(desiredSpeed);
    }

    // Below is the code for controlling the motor Spark1.
    enabled = SmartDashboard.getBoolean("Motor Spark1 Enabled", false);
    if (enabled && m_motorSpark1 == null) {
      m_motorSpark1 = new CANSparkMax((int) SmartDashboard.getNumber("Motor Spark1 Port", m_portMotorSpark1),
          MotorType.kBrushed);
    } else if (!enabled && m_motorSpark1 != null) {
      m_motorSpark1.set(0.0);
      m_motorSpark1.close();
      m_motorSpark1 = null;
    }

    desiredPort = (int) SmartDashboard.getNumber("Motor Spark1 Port", m_portMotorSpark1);
    if (desiredPort != m_portMotorSpark1 && enabled) {
      m_motorSpark1 = new CANSparkMax(desiredPort, MotorType.kBrushed);
      m_portMotorSpark1 = desiredPort;
    }

    desiredSpeed = SmartDashboard.getNumber("Motor Spark1 Speed", 0);
    if ((desiredSpeed < -1.0 || desiredSpeed > 1.0) && enabled) {
      m_motorSpark1.set(0.0);
      SmartDashboard.putNumber("Motor Spark1 Speed", 0);
    } else if (enabled) {
      m_motorSpark1.set(desiredSpeed);
    }

    // Below is the code for controlling the motor Spark2.
    enabled = SmartDashboard.getBoolean("Motor Spark2 Enabled", false);
    if (enabled && m_motorSpark2 == null) {
      m_motorSpark2 = new CANSparkMax((int) SmartDashboard.getNumber("Motor Spark2 Port", m_portMotorSpark2),
          MotorType.kBrushed);
    } else if (!enabled && m_motorSpark2 != null) {
      m_motorSpark2.set(0.0);
      m_motorSpark2.close();
      m_motorSpark2 = null;
    }

    desiredPort = (int) SmartDashboard.getNumber("Motor Spark2 Port", m_portMotorSpark2);
    if (desiredPort != m_portMotorSpark2 && enabled) {
      m_motorSpark2 = new CANSparkMax(desiredPort, MotorType.kBrushed);
      m_portMotorSpark2 = desiredPort;
    }

    desiredSpeed = SmartDashboard.getNumber("Motor Spark2 Speed", 0);
    if ((desiredSpeed < -1.0 || desiredSpeed > 1.0) && enabled) {
      m_motorSpark2.set(0.0);
      SmartDashboard.putNumber("Motor Spark2 Speed", 0);
    } else if (enabled) {
      m_motorSpark2.set(desiredSpeed);
    }
  }

  @Override
  public void simulationPeriodic() {
    // This method will be called once per scheduler run during simulation
  }
}
